/**
 * Controller Dice Tester
 *
 * @author River Vaughn Vink
 * @since 03-07-2017
 * @version 1.1
 */

package sample;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.util.Duration;
import java.text.DecimalFormat;

/**
 * Controller - allows user to use program
 */
public class Controller extends Application {
    //Setup model and view
    private Model model = new Model(1000,6);
    private View view = new View(model);
    private boolean isWorking = true;
    private boolean isChart = true;


    /**
     * Starts program
     *
     * @param primaryStage scene object
     */
    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Dice Tester");
        primaryStage.setScene(new Scene(root));
        //start
        primaryStage.show();
    }
    //Declare fxml variables
    @FXML
    private Slider slider;
    @FXML
    private ImageView diceImg;
    @FXML
    private Label sliderText;
    @FXML
    private BarChart chart;
    @FXML
    private Button pauseButton;
    @FXML
    private ChoiceBox chBox;
    @FXML
    private Label statSmall;
    @FXML
    private TableView table;
    @FXML
    private TableColumn col1;
    @FXML
    private TableColumn col2;
    /**
     * Runs after FXML injection
     */
    public void initialize(){
        //start main loop
        view.startLoop();
        sliderText.setText(Double.toString((int) slider.getValue())+" ms");
        //add event listeners
        slider.valueProperty().addListener((observable, oldValue, newValue) -> {
            sliderText.setText(Double.toString(newValue.intValue())+" ms");
            model.setIterationInterval((int) slider.getValue());
            if(isWorking) {
                view.stopLoop();
                view.startLoop();
            }
        });
        chBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            String val = chBox.getValue().toString();
            System.out.println(val);
            if(val.equals("Cube")){
                model.setNumberOfSides(6);
            } else if(val.equals("Dodecahedron")){
                model.setNumberOfSides(12);
            } else if(val.equals("Icosahedron")){
                model.setNumberOfSides(20);
            }
        });
        //prepare chart
        XYChart.Series<String, Number> series = new XYChart.Series<>();
        series.setName("Frequency");
        for (int i = 1; i < 21; i++) {
            series.getData().add(new XYChart.Data<>(Integer.toString(i), model.occurrences(i)));
        }
        chart.getData().addAll(series);
        //Start secondary loop
        updateStatistics();
    }
    /**
     * Triggered when "reset" button is pressed, deletes arraylist
     */
    public void resetButton() {
        model.resetList();
    }
    /**
     * Serves as table row
     */
    public class twoVar {
        private int a;
        private int b;
        /**
         * Constructor of the twoVar class
         *
         * @param a first column value
         * @param b second column value
         */
        twoVar(int a, int b){
            this.a = a;
            this.b = b;
        }
        public int getA() {
            return a;
        }

        public int getB() {
            return b;
        }
    } //Object used as table row
    /**
     * Connected to switch button, switches between chart and table
     */
    public void switchView(){
        if(isChart){
            isChart = false;
            table.setVisible(true);
            chart.setVisible(false);
        } else {
            table.setVisible(false);
            chart.setVisible(true);
            isChart = true;
        }
    }
    /**
     * General update loop, only used for statistical updates
     */
    public void updateStatistics(){
        //Executed on main FXML thread
        Timeline fiveSecondsWonder = new Timeline(new KeyFrame(Duration.millis(50), new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                //print total and average
                String total = Integer.toString(model.iterationCount());
                String average = Double.toString(model.calculateAverage(model.getAl()));
                DecimalFormat decimalFormat = new DecimalFormat("0.00");
                String format = decimalFormat.format(model.calculateAverage(model.getAl()));
                //Attach to Label
                statSmall.setText("Average: "+ format +" Total: " + total);
                //Update die image
                diceImg.setImage(new Image("file:" + Integer.toString(view.getLastDice()) +".jpg"));
                //Clear chart
                chart.getData().clear();
                chart.layout();
                //Update chart
                XYChart.Series<String, Number> series = new XYChart.Series<>();
                series.setName("Frequency");
                for (int i = 1; i < 21; i++) {
                    series.getData().add(new XYChart.Data<>(Integer.toString(i), model.occurrences(i)));
                }
                chart.getData().addAll(series); //Setting the data to bar chart
                //Update table
                ObservableList<twoVar> data = FXCollections.observableArrayList();
                for (int i = 1; i < 21; i++) {
                    data.add(new twoVar(i,(int)model.occurrences(i)));
                }
                col1.setCellValueFactory(
                        new PropertyValueFactory<>("a"));
                col2.setCellValueFactory(
                        new PropertyValueFactory<>("b"));
                //Set data to table
                table.setItems(data);
            }
        }));
        fiveSecondsWonder.setCycleCount(Timeline.INDEFINITE);
        fiveSecondsWonder.play();
    }
    /**
     * Connected to the resume/pause button, stops/starts main loop
     */
    public void buttonPressed() {
        if(isWorking){
            pauseButton.setText("Resume");
            isWorking = false;
            view.stopLoop();
        } else {
            pauseButton.setText("Pause");
            view.startLoop();
            isWorking = true;
        }
    }

    /**
     * Overrides default exit method
     */
    public void stop(){
        System.exit(2);
    }
    /**
     * Launches other functions
     *
     * @param args Arguments passed by program parameters (not used)
     */
    public void main(String[] args) {

        launch(args);
    }
}
