/**
 * View Dice Tester
 *
 * @author River Vaughn Vink
 * @since 03-07-2017
 * @version 1.1
 */
package sample;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * View class - holds main processes
 */
public class View {
    //Timer object
    private Timer timer;
    private int lastDice;
    Model model;
    /**
     * Constructor for model class
     *
     * @param model model to be used, default delivered by controller
     */
    View(Model model){
       this.model = model;
    }
    /**
     * Generate a random number between min and max
     *
     * @param minimum minimum value
     * @param maximum maximum value
     * @return random number
     */
    public int genRand(int minimum, int maximum){
        Random r = new Random();
        return  r.nextInt(maximum-minimum) + minimum;
    }
    /**
     * Stops main loop
     */
    public void stopLoop(){
        timer.cancel();
    }
    /**
     * Starts main loop
     */
    public void startLoop(){
        //create new timer
        timer = new Timer();
        class DiceThrow extends TimerTask {
            public void run() {
                //random facing
                int result = genRand(1,model.getNumberOfSides()+1);
                //add to arrayList
                model.getAl().add(result);
                lastDice = result;
                //save result
                model.saveBuffer(model.getLogFile(), Integer.toString(result)+":"+ model.getNumberOfSides() +"\n");
                System.out.println(result);
            }
        }
        timer.schedule(new DiceThrow(), 0, model.getIterationInterval());
    }
    /**
     * Getter for the last dice result
     *
     * @return last dice facing
     */
    public int getLastDice() {
        return lastDice;
    }
}
