/**
 * Model Dice Tester
 *
 * @author River Vaughn Vink
 * @since 03-07-2017
 * @version 1.1
 */
package sample;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.*;
/**
 * Model class - holds parameters and data.
 */
public class Model {
    private int iterationInterval;
    private int numberOfSides;
    //Destination to save logs
    private String logFile = "diceLog.txt";
    private ArrayList al = new ArrayList();
    /**
     * Constructor of the model class
     *
     * @param iterationInterval time between iterations in ms
     * @param numberOfSides amount of die facings
     */
    Model(int iterationInterval, int numberOfSides){
        this.iterationInterval = iterationInterval;
        this.numberOfSides = numberOfSides;
    }
    /**
     * Calculates basic average
     *
     * @param marks List of all values
     * @return average
     */
    public double calculateAverage(List<Integer> marks) {
        Integer sum = 0;
        if(!marks.isEmpty()) {
            for (Integer mark : marks) {
                sum += mark;
            }
            return sum.doubleValue() / marks.size();
        }
        return sum;
    }
    /**
     * save a die roll to the log
     *
     * @param fileName name of the file
     * @param userBuffer string to be saved
     */
    public void saveBuffer(String fileName, String userBuffer){
        //System.out.println ("Saving..");
        try {
            PrintStream out = new PrintStream(new FileOutputStream(fileName, true));
            out.print(userBuffer);
            out.close();
        }
        //if failed
        catch (FileNotFoundException exc){
            System.out.println ("Saving failed");
        }
    }
    /**
     * Clears the arraylist
     */
    public void resetList(){
        al.clear();
    }
    /**
     * Returns arrayList size
     * @return arrayLise Size
     */
    public int iterationCount(){
        return al.size();
    }
    /**
     * Counts the amount of occurrences of o in arrayList
     *
     * @param o integer to count
     * @return occurrences
     */
    public double occurrences(int o){
        return Collections.frequency(al, o);
    }
    /**
     * Getter for arrayList
     * @return arraylist
     */
    public ArrayList getAl() {return al;}
    /**
     * Getter for logFile
     * @return logfile path
     */
    public String getLogFile() {
        return logFile;
    }
    /**
     * Getter for number of sides
     * @return number of facings
     */
    public int getNumberOfSides() {
        return numberOfSides;
    }
    /**
     * Set number of sides to int
     * @param numberOfSides amount of new facings
     */
    public void setNumberOfSides(int numberOfSides) {
        this.numberOfSides = numberOfSides;
    }
    /**
     * Getter for iteration timeout
     * @return time between iterations
     */
    public int getIterationInterval() {
        return iterationInterval;
    }
    /**
     * Setter for iteration timeout
     * @param iterationInterval time in ms
     */
    public void setIterationInterval(int iterationInterval) {
        this.iterationInterval = iterationInterval;
    }
}
